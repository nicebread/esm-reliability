## ======================================================================
## Compute scale inter-correlations on three levels:
## Between-person, within-person/between-days, within-person between-moments
## To control for nested structure, subtract all higher unit means
## ======================================================================

library(dplyr)
library(tibble)

load(file="processed_data/S1.RData")
load(file="processed_data/S2.RData")
load(file="processed_data/varDecomp.RData") # contains the reliabilities for disattenuation

# load all files in the /helpers directory
R.utils::sourceDirectory("helpers/", modifiedOnly=TRUE)



# consecutively centers a variable from the highest to the lowest level:
# first remove couple mean (var.cC), then additionally remove person mean (var.cC.cP), then remove additionally day mean (var.cC.cP.cD)
removeMeans <- function(dat, v) {
  longData <- dat %>% as.data.frame()
  longData[, paste0(v, ".original")] <- longData[, v]

  # remove couple mean from persons
  longData[, paste0(v, ".couple.M")] <- ave(longData[, v], longData$couple_uid, FUN=function(x) mean(x, na.rm=TRUE))
  longData[, paste0(v, ".cC")] <- longData[, v] - longData[, paste0(v, ".couple.M")]

  # remove person mean from days
  longData[, paste0(v, ".person.M")] <- ave(longData[, paste0(v, ".cC")], longData$person_uid, FUN=function(x) mean(x, na.rm=TRUE))
  longData[, paste0(v, ".cC.cP")] <- longData[, paste0(v, ".cC")] - longData[, paste0(v, ".person.M")]

  # remove day mean from moments
  longData[, paste0(v, ".day.M")] <- ave(longData[, paste0(v, ".cC.cP")], longData$person_studyday_uid, FUN=function(x) mean(x, na.rm=TRUE))
  longData[, paste0(v, ".cC.cP.cD")] <- longData[, paste0(v, ".cC.cP")] - longData[, paste0(v, ".day.M")]

  vs <- c(paste0(v, ".original"), paste0(v, ".couple.M"), paste0(v, ".cC"), paste0(v, ".person.M"), paste0(v, ".cC.cP"), paste0(v, ".day.M"), paste0(v, ".cC.cP.cD"))

  longData2 <- longData %>%
    select("couple_uid", "person_uid", "person_studyday_uid", "moment_uid", one_of(vs))

  return(longData2)
}


# join all centered variables together
S1.centered <- Reduce(function(...) merge(..., by=c("couple_uid", "person_uid", "person_studyday_uid", "moment_uid"), all.x=TRUE), list(
  removeMeans(dat=S1, v="RS2"),
  removeMeans(dat=S1, v="Ind"),
  removeMeans(dat=S1, v="Pow"),
  removeMeans(dat=S1, v="A"),
  removeMeans(dat=S1, v="C2"),
  removeMeans(dat=S1, v="C3"),
  removeMeans(dat=S1, v="C4")
))

S2.centered <- Reduce(function(...) merge(..., by=c("couple_uid", "person_uid", "person_studyday_uid", "moment_uid"), all.x=TRUE), list(
  removeMeans(dat=S2, v="RS2"),
  removeMeans(dat=S2, v="RS3"),
  removeMeans(dat=S2, v="Ind"),
  removeMeans(dat=S2, v="Pow"),
  removeMeans(dat=S2, v="A"),
  removeMeans(dat=S2, v="C2"),
  removeMeans(dat=S2, v="C3"),
  removeMeans(dat=S2, v="C4")
))


# couple level: simply aggregate within each couple; no correction necessary
S1.couple.cor <- S1.centered %>%
  select(couple_uid, ends_with(".original"), - C2.original, - C3.original) %>%
  group_by(couple_uid) %>%
  summarise_all(mean, na.rm=TRUE) %>%
  ungroup() %>%
  select(-couple_uid) %>%
  cor(use="pairwise") %>% round(2)

# person level: remove couple mean to control for between-couple effects
S1.person.cor <- S1.centered %>%
  select(person_uid, ends_with(".cC"), - C2.cC, - C3.cC) %>%
  group_by(person_uid) %>%
  summarise_all(mean, na.rm=TRUE) %>%
  ungroup() %>%
  select(-person_uid) %>%
  cor(use="pairwise") %>% round(2)

# day level: remove couple and person means
S1.day.cor <- S1.centered %>%
  select(person_studyday_uid, ends_with(".cC.cP"), - C2.cC.cP, - C3.cC.cP) %>%
  group_by(person_studyday_uid) %>%
  summarise_all(mean, na.rm=TRUE) %>%
  ungroup() %>%
  select(-person_studyday_uid) %>%
  cor(use="pairwise") %>% round(2)

# moment level: remove couple, person, and day means
S1.moment.cor <- S1.centered %>%
  select(person_uid, moment_uid, ends_with(".cC.cP.cD"), - C2.cC.cP.cD, - C3.cC.cP.cD) %>%
  group_by(person_uid, moment_uid) %>%
  summarise_all(mean, na.rm=TRUE) %>%
  ungroup() %>%
  select(-person_uid, - moment_uid) %>%
  cor(use="pairwise") %>% round(2)


# add empty row and column for the missing RS3 scale
S1.couple.cor <- add_column(as.data.frame(S1.couple.cor), RS3=NA, .after=1)
S1.couple.cor <- add_row(as.data.frame(S1.couple.cor), .after=1)

S1.person.cor <- add_column(as.data.frame(S1.person.cor), RS3=NA, .after=1)
S1.person.cor <- add_row(as.data.frame(S1.person.cor), .after=1)

S1.day.cor <- add_column(as.data.frame(S1.day.cor), RS3=NA, .after=1)
S1.day.cor <- add_row(as.data.frame(S1.day.cor), .after=1)

S1.moment.cor <- add_column(as.data.frame(S1.moment.cor), RS3=NA, .after=1)
S1.moment.cor <- add_row(as.data.frame(S1.moment.cor), .after=1)

# pretty row & column names
rownames(S1.couple.cor) <- rownames(S1.person.cor) <- rownames(S1.day.cor) <- rownames(S1.moment.cor) <- colnames(S1.couple.cor) <- colnames(S1.person.cor) <- colnames(S1.day.cor) <- colnames(S1.moment.cor) <- c("RS2", "RS3", "Ind", "Pow", "A", "C")

# ---------------------------------------------------------------------
# Study 2

# couple level: simply aggregate within each couple; no correction necessary
S2.couple.cor <- S2.centered %>%
  select(couple_uid, ends_with(".original"), - C2.original, - C3.original) %>%
  group_by(couple_uid) %>%
  summarise_all(mean, na.rm=TRUE) %>%
  ungroup() %>%
  select(-couple_uid) %>%
  cor(use="pairwise") %>% round(2)

# person level: remove couple mean to control for between-couple effects
S2.person.cor <- S2.centered %>%
  select(person_uid, ends_with(".cC"), - C2.cC, - C3.cC) %>%
  group_by(person_uid) %>%
  summarise_all(mean, na.rm=TRUE) %>%
  ungroup() %>%
  select(-person_uid) %>%
  cor(use="pairwise") %>% round(2)

# day level: remove couple and person means
S2.day.cor <- S2.centered %>%
  select(person_studyday_uid, ends_with(".cC.cP"), - C2.cC.cP, - C3.cC.cP) %>%
  group_by(person_studyday_uid) %>%
  summarise_all(mean, na.rm=TRUE) %>%
  ungroup() %>%
  select(-person_studyday_uid) %>%
  cor(use="pairwise") %>% round(2)

# moment level: remove couple, person, and day means
S2.moment.cor <- S2.centered %>%
  select(person_uid, moment_uid, ends_with(".cC.cP.cD"), - C2.cC.cP.cD, - C3.cC.cP.cD) %>%
  group_by(person_uid, moment_uid) %>%
  summarise_all(mean, na.rm=TRUE) %>%
  ungroup() %>%
  select(-person_uid, - moment_uid) %>%
  cor(use="pairwise") %>% round(2)


# pretty row & column names
rownames(S2.couple.cor) <- rownames(S2.person.cor) <- rownames(S2.day.cor) <- rownames(S2.moment.cor) <- colnames(S2.couple.cor) <- colnames(S2.person.cor) <- colnames(S2.day.cor) <- colnames(S2.moment.cor) <- c("RS2", "RS3", "Ind", "Pow", "A", "C")


# ---------------------------------------------------------------------
#  RAW CORRELATIONS: combine upper half of S1 with upper half of S2; transpose to flip upper tri to lower tri

cor.couplelevel <- S1.couple.cor
cor.couplelevel[lower.tri(cor.couplelevel)] <- t(S2.couple.cor[lower.tri(S2.couple.cor)])

cor.personlevel <- S1.person.cor
cor.personlevel[lower.tri(cor.personlevel)] <- t(S2.person.cor[lower.tri(S2.person.cor)])

cor.daylevel <- S1.day.cor
cor.daylevel[lower.tri(cor.daylevel)] <- t(S2.day.cor[lower.tri(S2.day.cor)])

cor.momentlevel <- S1.moment.cor
cor.momentlevel[lower.tri(cor.momentlevel)] <- t(S2.moment.cor[lower.tri(S2.moment.cor)])

# format for printing
cor.couplelevel.string <- f2(cor.couplelevel, digits=2, skipZero=TRUE, trimToZero=.005, suppressNA=TRUE)
cor.personlevel.string <- f2(cor.personlevel, digits=2, skipZero=TRUE, trimToZero=.005, suppressNA=TRUE)
cor.daylevel.string <- f2(cor.daylevel, digits=2, skipZero=TRUE, trimToZero=.005, suppressNA=TRUE)
cor.momentlevel.string <- f2(cor.momentlevel, digits=2, skipZero=TRUE, trimToZero=.005, suppressNA=TRUE)
diag(cor.couplelevel.string) <- diag(cor.personlevel.string) <- diag(cor.daylevel.string) <- diag(cor.momentlevel.string) <- ""
rownames(cor.couplelevel.string) <- rownames(cor.personlevel.string) <- rownames(cor.daylevel.string) <- rownames(cor.momentlevel.string) <- colnames(cor.couplelevel.string)



save(cor.couplelevel, cor.personlevel, cor.daylevel, cor.momentlevel,
     cor.couplelevel.string, cor.personlevel.string, cor.daylevel.string, cor.momentlevel.string,
     file="processed_data/corTabs.RData")


# ---------------------------------------------------------------------
# Compare correlations between levels - signs for ecological fallacy, or generally a pattern of ergodicity?

# difference between person and daylevel
p.d.diffs <- as.vector(as.matrix(abs(cor.personlevel - cor.daylevel)))
psych::describe(p.d.diffs)
hist(p.d.diffs)

# no substantial difference between day- and moment-level:
d.m.diffs <- as.vector(as.matrix(abs(cor.momentlevel - cor.daylevel)))
psych::describe(d.m.diffs)
hist(d.m.diffs)
