# Overview of variance components

Overall we have 23 variance components in Eq 1 (including error). This document systematically specifies which terms contribute to true score variance and observed variance for all reliability coefficients.

## General assumptions:

These factors (and their interactions) are assumed to fixed --> entirely ignored in reliability computation:

- moment
- item
- moment:item

All factors which include terms that are on a higher level than the focal term (e.g., all terms including couple when we focus on between-person reliability) are ignored. All random terms (including interaction terms with at least one random component) go into the denominator, as they are a potential source of unsystematic variation.

Additional assumptions are specified below.

## Between couple reliability

### focal terms (all fixed, except couple) --> numerator

Variance component | Comment
-------------------|-------------------------
couple             | focal variance component
couple:moment      |
couple:item        |
couple:moment:item |


### random terms (i.e., at least one random term in interaction) --> denominator

Variance component     | Comment
-----------------------|----------------------------------
couple:day             |
couple:day:moment      |
couple:day:item        |
couple:day:moment:item |
person                 |
day                    |
person:day             |
person:moment          |
person:item            |
day:moment             |
day:item               |
person:day:moment      |
person:day:item        |
person:moment:item     |
day:moment:item        |
moment                 | ignored (see general assumptions)
item                   | ignored (see general assumptions)
moment:item            | ignored (see general assumptions)
error                  |




## Between person reliability

### Ignored terms

Variance component     | Comment
-----------------------|-----------------------
couple                 | ignored (higher level, irrelevant for reliability on the focal level)
couple:moment          | ignored (higher level, irrelevant for reliability on the focal level)
couple:item            | ignored (higher level, irrelevant for reliability on the focal level)
couple:moment:item     | ignored (higher level, irrelevant for reliability on the focal level)
couple:day             | ignored (higher level, irrelevant for reliability on the focal level)
couple:day:moment      | ignored (higher level, irrelevant for reliability on the focal level)
couple:day:item        | ignored (higher level, irrelevant for reliability on the focal level)
couple:day:moment:item | ignored (higher level, irrelevant for reliability on the focal level)

### focal (all fixed, except person) --> numerator

Variance component | Comment
-------------------|-------------------------
person             | focal variance component
person:moment      |
person:item        |
person:moment:item |

### random (at least one term in interaction) --> denominator

Variance component | Comment
-------------------|----------------------------------
day                |
person:day         |
day:moment         |
day:item           |
person:day:moment  |
person:day:item    |
day:moment:item    |
moment             | ignored (see general assumptions)
item               | ignored (see general assumptions)
moment:item        | ignored (see general assumptions)
error              |



## Within-person, between day reliability

### Ignored terms (n=12)

Variance component     | Comment
-----------------------|-----------------------
couple                 | ignored (higher level, irrelevant for reliability on the focal level)
couple:moment          | ignored (higher level, irrelevant for reliability on the focal level)
couple:item            | ignored (higher level, irrelevant for reliability on the focal level)
couple:moment:item     | ignored (higher level, irrelevant for reliability on the focal level)
couple:day             | ignored (higher level, irrelevant for reliability on the focal level)
couple:day:moment      | ignored (higher level, irrelevant for reliability on the focal level)
couple:day:item        | ignored (higher level, irrelevant for reliability on the focal level)
couple:day:moment:item | ignored (higher level, irrelevant for reliability on the focal level)
person:moment          | ignored (higher level, irrelevant for reliability on the focal level)
person                 | ignored (higher level, irrelevant for reliability on the focal level)
person:item            | ignored (higher level, irrelevant for reliability on the focal level)
person:moment:item     | ignored (higher level, irrelevant for reliability on the focal level)

### focal (all fixed, except person:day) --> numerator

Variance component | Comment
-------------------|-------------------------
person:day         | focal variance component
person:day:moment  |
person:day:item    |

(note: person:day:moment:item does not exist as a factor, as there is only one replication (i.e., only one item) on this level)

### random (at least one term in interaction) --> denominator

Variance component | Comment
-------------------|----------------------------------
day                |
day:moment         |
day:item           |
day:moment:item    |
moment             | ignored (see general assumptions)
item               | ignored (see general assumptions)
moment:item        | ignored (see general assumptions)
error              |




## Within-person, within-days, between moments reliability

### Ignored terms (n=18)

Variance component     | Comment
-----------------------|-----------------------
couple                 | ignored (higher level, irrelevant for reliability on the focal level)
couple:moment          | ignored (higher level, irrelevant for reliability on the focal level)
couple:item            | ignored (higher level, irrelevant for reliability on the focal level)
couple:moment:item     | ignored (higher level, irrelevant for reliability on the focal level)
couple:day             | ignored (higher level, irrelevant for reliability on the focal level)
couple:day:moment      | ignored (higher level, irrelevant for reliability on the focal level)
couple:day:item        | ignored (higher level, irrelevant for reliability on the focal level)
couple:day:moment:item | ignored (higher level, irrelevant for reliability on the focal level)
person:moment          | ignored (higher level, irrelevant for reliability on the focal level)
person                 | ignored (higher level, irrelevant for reliability on the focal level)
person:item            | ignored (higher level, irrelevant for reliability on the focal level)
person:moment:item     | ignored (higher level, irrelevant for reliability on the focal level)
person:day             | ignored (higher level, irrelevant for reliability on the focal level)
person:day:item        | ignored (higher level, irrelevant for reliability on the focal level)
day                    | ignored (higher level, irrelevant for reliability on the focal level)
day:moment             | ignored (as moment is assumed to be a fixed factor)
day:item               | ignored (as item is assumed to be a fixed factor)
day:moment:item        | ignored (as item and moment are assumed to be fixed factors)

### focal (all fixed, except person:day:moment ) --> numerator

Variance component | Comment
-------------------|-------------------------
person:day:moment  | focal variance component

(note: person:day:moment:item does not exist as a factor, as there is only one replication (i.e., only one item) on this level)

### random (at least one term in interaction) --> denominator

Variance component | Comment
-------------------|----------------------------------
moment             | ignored (see general assumptions)
item               | ignored (see general assumptions)
moment:item        | ignored (see general assumptions)
error              |


