## ======================================================================
## VARIANCE DECOMPOSITION, treating dyad members as indistinguishable
## source("3-Variance decomposition.R", echo=TRUE)
## ======================================================================

library(lme4)
options(scipen=999)

# THESE DYADIC DATA SETS CANNOT BE PROVIDED OPENLY FOR PRIVACY REASONS
load(file="processed_data/S1.long.RData")
load(file="processed_data/S2.long.RData")
load(file="processed_data/S2.unstand.long.RData")

source("helpers/model_syntax_indistinguishable.R")

# verbosity level
verb <- 1

# ---------------------------------------------------------------------
# Compute variance decomposition (VDC) for Study 1

VDC.S1.RS2 <- lmer(formula=VDC.indistinguishable.syntax, data=S1.RS2.long, verbose=verb, REML=FALSE)
VDC.S1.C2 <- lmer(formula=VDC.indistinguishable.syntax, data=S1.C2.long, verbose=verb, REML=FALSE)
VDC.S1.C3 <- lmer(formula=VDC.indistinguishable.syntax, data=S1.C3.long, verbose=verb, REML=FALSE)
VDC.S1.C4 <- lmer(formula=VDC.indistinguishable.syntax, data=S1.C4.long, verbose=verb, REML=FALSE)
VDC.S1.C2EJP <- lmer(formula=VDC.indistinguishable.syntax, data=S1.C2EJP.long, verbose=verb, REML=FALSE)
VDC.S1.A <- lmer(formula=VDC.indistinguishable.syntax, data=S1.A.long, verbose=verb, REML=FALSE)
VDC.S1.Pow <- lmer(formula=VDC.indistinguishable.syntax, data=S1.Pow.long, verbose=verb, REML=FALSE)
VDC.S1.Ind <- lmer(formula=VDC.indistinguishable.syntax, data=S1.Ind.long, verbose=verb, REML=FALSE)

# sanity check: Do all variances (except for VDC.S1.C2EJP) add up to (roughly) 1 (because of standardization)?
get_overall_variance <- function(model) sum(as.data.frame(lme4::VarCorr(model))$vcov)
sapply(list(VDC.S1.RS2, VDC.S1.Ind, VDC.S1.Pow, VDC.S1.A, VDC.S1.C2, VDC.S1.C3, VDC.S1.C4, VDC.S1.C2EJP), get_overall_variance)


save(VDC.S1.RS2, VDC.S1.Ind, VDC.S1.Pow, VDC.S1.A, VDC.S1.C2, VDC.S1.C3, VDC.S1.C4, VDC.S1.C2EJP, file="processed_data/VDC.S1.RData")


# ---------------------------------------------------------------------
# Compute variance decomposition (VDC) for Study 2

VDC.S2.RS2 <- lmer(formula=VDC.indistinguishable.syntax, data=S2.RS2.long, verbose=verb, REML=FALSE)
VDC.S2.RS3 <- lmer(formula=VDC.indistinguishable.syntax, data=S2.RS3.long, verbose=verb, REML=FALSE)
VDC.S2.C2 <- lmer(formula=VDC.indistinguishable.syntax, data=S2.C2.long, verbose=verb, REML=FALSE)
VDC.S2.C3 <- lmer(formula=VDC.indistinguishable.syntax, data=S2.C3.long, verbose=verb, REML=FALSE)
VDC.S2.C4 <- lmer(formula=VDC.indistinguishable.syntax, data=S2.C4.long, verbose=verb, REML=FALSE)
VDC.S2.A <- lmer(formula=VDC.indistinguishable.syntax, data=S2.A.long, verbose=verb, REML=FALSE)
VDC.S2.Pow <- lmer(formula=VDC.indistinguishable.syntax, data=S2.Pow.long, verbose=verb, REML=FALSE)
VDC.S2.Ind <- lmer(formula=VDC.indistinguishable.syntax, data=S2.Ind.long, verbose=verb, REML=FALSE)

# sanity check: Do all variances add up to (roughly) 1 (because of standardization)?
sapply(list(VDC.S2.RS2, VDC.S2.RS3, VDC.S2.Ind, VDC.S2.Pow, VDC.S2.A, VDC.S2.C2, VDC.S2.C3, VDC.S2.C4), get_overall_variance)

save(VDC.S2.RS2, VDC.S2.RS3, VDC.S2.Ind, VDC.S2.Pow, VDC.S2.A, VDC.S2.C2, VDC.S2.C3, VDC.S2.C4, file="processed_data/VDC.S2.RData")

# ---------------------------------------------------------------------
# Compute variance decomposition (VDC) for Study 2, for the MotBeh paper: Unstandardized items
# (Single item reliabilities are computed in _6-Single items.R)
# This is reported in footnote 4 of the paper

VDC.S2.unstand.C2 <- lmer(formula=VDC.indistinguishable.syntax, data=S2.C2.unstand.long, verbose=verb, REML=FALSE)
VDC.S2.unstand.RS2 <- lmer(formula=VDC.indistinguishable.syntax, data=S2.C2.unstand.long, verbose=verb, REML=FALSE)
VDC.S2.unstand.A <- lmer(formula=VDC.indistinguishable.syntax, data=S2.A.unstand.long, verbose=verb, REML=FALSE)
VDC.S2.unstand.Pow <- lmer(formula=VDC.indistinguishable.syntax, data=S2.Pow.unstand.long, verbose=verb, REML=FALSE)
VDC.S2.unstand.Ind <- lmer(formula=VDC.indistinguishable.syntax, data=S2.Ind.unstand.long, verbose=verb, REML=FALSE)

save(VDC.S2.unstand.C2, file="processed_data/VDC.S2.unstand.RData")

save(VDC.S2.unstand.Ind, VDC.S2.unstand.Pow, VDC.S2.unstand.A, VDC.S2.unstand.C2, file="processed_data/VDC.S2.unstand.RData")
