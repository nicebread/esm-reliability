# Accompanying source code for "A reliability model for moments, days, and persons nested in couples"

This is the accompanying source code for:

 Schönbrodt, F. D., Zygar, C., Nestler, S., Pusch, S., & Hagemeyer, B. (in press). Measuring motivational relationship processes in experience sampling: A reliability model for moments, days, and persons nested in couples. *Behavior Research Methods*. http://dx.doi.org/10.3758/s13428-021-01701-7. Retrieved from [psyarxiv.com/6mq7t](https://psyarxiv.com/6mq7t).

The lme4 model syntax for the variance decomposition is in [`helpers/model_syntax_distinguishable.R`](helpers/model_syntax_distinguishable.R).

## How to reproduce the paper

### Get the raw data

Due to the dyadic nature of the data set, we cannot make the data fully openly available. The data and materials for [Sample 1](https://doi.org/10.5160/psychdata.zrce16dy99) and [Sample 2](https://doi.org/10.5160/psychdata.zrce18mo99) are published as scientific use files, which restricts access to scientific users. Please go to these links and request access to the data at the PsychData data repository.

For reproducing the results, place the following raw data files from the PsychData repositories into the folder `/raw_sensitive`:

* couple_matching_data.csv
* zrce16dy99_fd1.csv
* zrce16dy99_fd2.csv
* zrce16dy99_fd3.csv
* zrce18mo99_fd1.tsv
* zrce18mo99_fd2.tsv
* zrce18mo99_fd3.tsv


### Reproduce the analyses (*with* access to the data)

1. Run `/preprocessing/source_basic_preprocessing.R` within that folder. This script takes the raw data files in `/raw_sensitive` and preprocesses them. Resulting processed data files are saved in `/processed_data`. Most relevant are `/processed_data/S1.dat.wide.RData` and `/processed_data/S2.dat.wide.RData` which contain the ESM data.
2. Run all R scripts in the top folder in the order provided (starting with `0a-Preprocessing_raw.R` and ending with `6-Scale correlations.R`)
   * `3a-Variance_decomposition.R` and `3b-Variance_decomposition_parallel.R` are alternative implementations for the variance estimation. The former does the computation sequentially (on a single core), the latter does it in parallel on 16 cores.
3. Compile `/paper_reliability/ESM_reliability_rev1.Rnw`. This computable manuscript does *not* do the actual computations, but rather loads the cached results from the scripts executed in step (2).

The script `00-Make.R` does all of these steps sequentially, assuming that you do parallel processing with at least 16 cores (you can switch to single core processing).
Depending on your processor speed, parallel processing needs around 12 hours, sequential processing around 3 days.

After all processing, the final computed paper is in `/paper_reliability/ESM_reliability_rev2.pdf`

## How to reproduce parts of the analyses (*without* access to the data)

Without access to the raw data files, scripts `0-Preprocessing.R` to `3b-Variance_decomposition_parallel.R` and `6-Scale_correlations.R` won't run.

Some shareable intermediate result files are located in `/processed_data`. 
Script `5-Reliability_computation.R` can be partly run, as it accesses intermediate data objects stored in `processed_data/varDecomp.RData`. Specifically, you can compute the reliabilities based on the number of scheduled pings, but not the reliability based on the actual number of pings (as this needs access to the sensitive raw data).

## A short tutorial on how to apply the (reduced formulas) to ***non-dyadic*** designs

You want to run a reliability estimation on non-dyadic data, where your data has moments within days within persons? 
You can find a short tutorial [here](demo_nondyadic/Demo_nondyadic.md) ([source code](demo_nondyadic/Demo_nondyadic.Rmd)).