#------------------------------------------------------------------------------
#  Full factorial model for distinguishable dyads
# All terms except the 5-way interaction are present.

VDC.distinguishable.syntax <- formula(
    value ~ 1 +
# main effects
    (1 | couple_id) +
    (1 | sex) +
    (1 | studyday_id) +
    (1 | moment_id) +
    (1 | item) +

# two-way IA
    (1 | couple_id:sex) +
    (1 | couple_id:studyday_id) +
    (1 | couple_id:moment_id) +
    (1 | couple_id:item) +

    (1 | sex:studyday_id) +
    (1 | sex:moment_id) +
    (1 | sex:item) +

    (1 | studyday_id:moment_id) +
    (1 | studyday_id:item) +

    (1 | moment_id:item) +

# three-way IA	
    (1 | couple_id:sex:studyday_id) +
    (1 | couple_id:sex:moment_id) +
    (1 | couple_id:sex:item) +
    (1 | couple_id:studyday_id:moment_id) +
    (1 | couple_id:studyday_id:item) +
    (1 | couple_id:moment_id:item) +

    (1 | sex:studyday_id:moment_id) +
    (1 | sex:studyday_id:item) +
    (1 | sex:moment_id:item) +
    (1 | studyday_id:moment_id:item) +

# four-way IA	
    (1 | couple_id:sex:studyday_id:moment_id) +
    (1 | couple_id:sex:studyday_id:item) +
    (1 | couple_id:sex:moment_id:item) +    
    (1 | couple_id:studyday_id:moment_id:item) +
    (1 | sex:studyday_id:moment_id:item)
)