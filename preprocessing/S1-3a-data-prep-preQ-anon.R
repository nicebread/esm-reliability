# ---------------------------------------------------------------------
# load packages

library(rio)
library(dplyr)
library(robustbase)
library(reshape2)


# ---------------------------------------------------------------------
# read data

preQ.dat <- import(file="../raw_sensitive/zrce16dy99_fd1.csv", encoding = "UTF-8", sep=",", na.strings="NA")
pact.dat <- import(file="../raw_sensitive/zrce16dy99_fd2.csv", encoding = "UTF-8", sep=",", na.strings="NA")
couples <- read.csv("../raw_sensitive/couple_matching_data.csv")

## ======================================================================
## correct faulty values
## ======================================================================

preQ.dat$sex[preQ.dat$code_new == 86449252] <- 2

## ======================================================================
## exclude data
## ======================================================================

# ---------------------------------------------------------------------
# remove test and empty cases

preQ.dat <- preQ.dat %>% filter(ended == TRUE)

# ---------------------------------------------------------------------
# exclude participant that completed the questionnaire via smartphone at first

preQ.dat <- preQ.dat %>% filter(session != "r5-xnlZYMGJEWDGdYAieFdvN7zsKHUTRSBpzLpMszCihjPqVPB1pNLmqChx0cYfZ")

# ---------------------------------------------------------------------
# exclude participants who aren't a couple

preQ.dat <- preQ.dat %>% filter(session != "UYypRZMN3ZWi-qRvGHQX64CF0Ys4BuDOJ7qDBQAxtVgliAJfLKXEXhuW0o8v-h3n" & session != "VKQcDdUvHUzPtJ5YjGr6y-fkAqFpMq2UnBpfyx2LO8WNftWdFwuvMj7DRXaE48gZ")

# ---------------------------------------------------------------------
# remove empty and irrelevant columns

sensitive_columns <- c("created", "modified", "expired", "teilnahme", "birth", "stud", "orient", "kinder", "kinderp", "jbez", "mbez", "bezstatus", "lat", "fern", "sehen", "schule", "schule_o", "beruf", "hwerk", "hwochenend", "pact1a", "pact1b", "pact1c", "pact1d", "pact2a", "pact2b", "pact2c", "pact2d", "pact3a", "pact3b", "pact3c", "pact3d", "pact4a", "pact4b", "pact4c", "pact4d", "pact5a", "pact5b", "pact5c", "pact5d", "pact6a", "pact6b", "pact6c", "pact6d", "pact7b", "pact7c", "pact7d", "pact8a", "pact8b", "pact8c", "pact8d")

preQ.dat <- preQ.dat %>% select(-sensitive_columns, -ended)

## ======================================================================
## add couple id
## ======================================================================

preQ.dat <- merge(preQ.dat, couples, by="code_new")
preQ.dat$paarid <- as.factor(preQ.dat$paarid)

## ======================================================================
## add time and age
## ======================================================================

# TODO
# time <- read.csv2("Preq/time-preq.csv")
# preQ.dat <- merge(preQ.dat, time, by = "code")
# 
# age <- read.csv2("Preq/age-preq.csv")
# preQ.dat <- merge(preQ.dat, age, by = "code")
                  
## ======================================================================
## compute scales
## ======================================================================

# ---------------------------------------------------------------------
# recode items

preQ.dat <- preQ.dat %>% group_by(code_new) %>% mutate(
  csi_1 = csi_1-1,
  csi_5.r = (csi_5-6)*-1,
  csi_26.r = (csi_26-5)*-1,
  csi_28.r = (csi_28-5)*-1,
  csi_31.r = (csi_31-5)*-1,
  csi = sum(csi_1, csi_5.r, csi_9, csi_11, csi_12, csi_17, csi_19, csi_20, csi_21, csi_22, csi_26.r, csi_27, csi_28.r, csi_30, csi_31.r, csi_32)
) %>% ungroup()

preQ.dat$abc_A <- as.numeric(sub(",", ".", as.character(preQ.dat$abc_A), fixed = TRUE))
preQ.dat$abc_B <- as.numeric(sub(",", ".", as.character(preQ.dat$abc_B), fixed = TRUE))
preQ.dat$abc_C <- as.numeric(sub(",", ".", as.character(preQ.dat$abc_C), fixed = TRUE))

# ---------------------------------------------------------------------
# create sex contrast and dummy variables

# sexContr: -1 = female, 1 = male (sex: 1 = male, 2 = female)
preQ.dat$sexContr <- preQ.dat$sex
preQ.dat$sexContr[preQ.dat$sexContr == 2] <- -1 ## sexContr: 1 = male, -1 = female

# sex dummy variables
preQ.dat$sexf <- 0
preQ.dat$sexf[preQ.dat$sexContr == -1] <- 1
preQ.dat$sexm <- 1-preQ.dat$sexf

# ---------------------------------------------------------------------
# compute scales

preQ.dat <- preQ.dat %>% group_by(code_new) %>% mutate(
  aut = sum(aut1, aut2)/2,
  int = sum(int1, int2)/2,
  dom = sum(dom1, dom2)/2,
  Pdom1.r = (dom1*-1)+6,
  Pdom2.r = (dom2*-1)+6,
  Pdom.r = sum(Pdom1.r, Pdom2.r)/2, 
  fabc4.r = (fabc4*-1)+8,
  fabc5.r = (fabc5*-1)+8,
  fabc6.r = (fabc6*-1)+8,
  fabc7.r = (fabc7*-1)+8,
  fabc_B = sum(fabc1, fabc3, fabc5.r, fabc7.r)/4,
  fabc_C = sum(fabc2, fabc4.r, fabc6.r, fabc8)/4,
  swls = sum(swls_1, swls_2, swls_3, swls_4, swls_5)/5,
  bwdm_1 = bwdm_1*-1+3,
  bwdm_2 = bwdm_2*-1+3,
  bwdm_3 = bwdm_3*-1+3,
  bwdm_4 = bwdm_4*-1+3,
  bwdm_5 = bwdm_5*-1+3,
  bwdm_6 = bwdm_6*-1+3,
  bwdm_7 = bwdm_7*-1+3,
  bwdm_8 = bwdm_8*-1+3,
  bwdm = sum(bwdm_1, bwdm_2, bwdm_3, bwdm_4, bwdm_5, bwdm_6, bwdm_7, bwdm_8)/8,
  ums_9.r = (ums_9-7)*-1,
  ums_13.r = (ums_13-7)*-1,
  UMS.Pow = (ums_1 + ums_5 + ums_9.r + ums_18 + ums_22 + ums_25)/6,
  UMS.Aff = (ums_2 + ums_6 + ums_10 + ums_13.r + ums_15 + ums_20)/6,
  UMS.Int = (ums_4 + ums_8 + ums_12 + ums_21 + ums_24 + ums_30)/6,
  UMS.Fear = (ums_11 + ums_14 + ums_16)/3, # careful, this is not the validated fear scale
  bfi_1.r = (bfi_1-6)*-1,
  bfi_3.r = (bfi_3-6)*-1,
  bfi_4.r = (bfi_4-6)*-1,
  bfi_5.r = (bfi_5-6)*-1,
  bfi_7.r = (bfi_7-6)*-1,
  bfi_n = sum(bfi_4.r, bfi_9)/2,
  bfi_e = sum(bfi_1.r, bfi_6)/2,
  bfi_o = sum(bfi_5.r, bfi_10)/2,
  bfi_v = sum(bfi_2, bfi_7.r)/2,
  bfi_g = sum(bfi_3.r, bfi_8)/2,
  prq = sum(pnrq_1, pnrq_2, pnrq_3, pnrq_4, pnrq_5, pnrq_6, pnrq_7, pnrq_8)/8,
  nrq = sum(pnrq_9, pnrq_10, pnrq_11, pnrq_12, pnrq_13, pnrq_14, pnrq_15, pnrq_16)/8
  ) %>% ungroup

# ---------------------------------------------------------------------
# z-standardize variables

preQ.dat <- preQ.dat %>% mutate(
  UMS.Pow.z = (preQ.dat$UMS.Pow - mean(preQ.dat$UMS.Pow, na.rm = TRUE))/sd(preQ.dat$UMS.Pow, na.rm = TRUE),
  UMS.Aff.z = (preQ.dat$UMS.Aff - mean(preQ.dat$UMS.Aff, na.rm = TRUE))/sd(preQ.dat$UMS.Aff, na.rm = TRUE),
  UMS.Int.z = (preQ.dat$UMS.Int - mean(preQ.dat$UMS.Int, na.rm = TRUE))/sd(preQ.dat$UMS.Int, na.rm = TRUE),
  UMS.Fear.z = (preQ.dat$UMS.Fear - mean(preQ.dat$UMS.Fear, na.rm = TRUE))/sd(preQ.dat$UMS.Fear, na.rm = TRUE),
  abc_A.z = (preQ.dat$abc_A - mean(preQ.dat$abc_A, na.rm = TRUE))/sd(preQ.dat$abc_A, na.rm = TRUE),
  abc_B.z = (preQ.dat$abc_B - mean(preQ.dat$abc_B, na.rm = TRUE))/sd(preQ.dat$abc_B, na.rm = TRUE),
  abc_C.z = (preQ.dat$abc_C - mean(preQ.dat$abc_C, na.rm = TRUE))/sd(preQ.dat$abc_C, na.rm = TRUE),
  csi.z = (preQ.dat$csi - mean(preQ.dat$csi, na.rm = TRUE))/sd(preQ.dat$csi, na.rm = TRUE),
  prq.z = (preQ.dat$prq - mean(preQ.dat$prq, na.rm = TRUE))/sd(preQ.dat$prq, na.rm = TRUE),
  nrq.z = (preQ.dat$nrq - mean(preQ.dat$nrq, na.rm = TRUE))/sd(preQ.dat$nrq, na.rm = TRUE),
  bfi_o.z = (preQ.dat$bfi_o - mean(preQ.dat$bfi_o, na.rm = TRUE))/sd(preQ.dat$bfi_o, na.rm = TRUE),
  bfi_g.z = (preQ.dat$bfi_g - mean(preQ.dat$bfi_g, na.rm = TRUE))/sd(preQ.dat$bfi_g, na.rm = TRUE),
  bfi_e.z = (preQ.dat$bfi_e - mean(preQ.dat$bfi_e, na.rm = TRUE))/sd(preQ.dat$bfi_e, na.rm = TRUE),
  bfi_v.z = (preQ.dat$bfi_v - mean(preQ.dat$bfi_v, na.rm = TRUE))/sd(preQ.dat$bfi_v, na.rm = TRUE),
  bfi_n.z = (preQ.dat$bfi_n - mean(preQ.dat$bfi_n, na.rm = TRUE))/sd(preQ.dat$bfi_n, na.rm = TRUE),
  swls.z = (preQ.dat$swls - mean(preQ.dat$swls, na.rm = TRUE))/sd(preQ.dat$swls, na.rm = TRUE),
  bwdm.z = (preQ.dat$bwdm - mean(preQ.dat$bwdm, na.rm = TRUE))/sd(preQ.dat$bwdm, na.rm = TRUE),
  bwdm.sex.z = (preQ.dat$bwdm_8 - mean(preQ.dat$bwdm_8, na.rm = TRUE))/sd(preQ.dat$bwdm_8, na.rm = TRUE),
  bwdm.time.z = (preQ.dat$bwdm_4 - mean(preQ.dat$bwdm_4, na.rm = TRUE))/sd(preQ.dat$bwdm_4, na.rm = TRUE),
  dom.z = (preQ.dat$dom - mean(preQ.dat$dom, na.rm = TRUE))/sd(preQ.dat$dom, na.rm = TRUE),
  Pdom.r.z = (preQ.dat$Pdom.r - mean(preQ.dat$Pdom.r, na.rm = TRUE))/sd(preQ.dat$Pdom.r, na.rm = TRUE)
) 

## ======================================================================
## pact data
## ======================================================================

# ---------------------------------------------------------------------
# exclude 0-codings for couple where both partners wrote the same story

pact.not.code_newd <- pact.dat %>% filter(code_new == "10625926" | code_new == "30318512")
pact.dat <- pact.dat %>% filter(code_new != "10625926" & code_new != "30318512")

for (i in 1:522){
  pact.not.code_newd[pact.not.code_newd$code_new == "30318512", i] <- NA
  pact.not.code_newd[pact.not.code_newd$code_new == "10625926", i] <- NA
}

# ---------------------------------------------------------------------
# compute residualized scores

## for EJP: lm() without na.action and setting, afterwards: lmrob()

pact.dat$IMot.C <- resid(lmrob(Communion ~ 1 + nwords, data = pact.dat, na.action=na.exclude, setting = "KS2014"))
pact.dat$IMot.C.Ap <- resid(lmrob(CommunionAp ~ 1 + nwords, data = pact.dat, na.action=na.exclude, setting = "KS2014"))
pact.dat$IMot.C.Av <- resid(lmrob(CommunionAv ~ 1 + nwords, data = pact.dat, na.action=na.exclude, setting = "KS2014"))
pact.dat$IMot.A <- resid(lmrob(Agency ~ 1 + nwords, data = pact.dat, na.action=na.exclude, setting = "KS2014"))
pact.dat$IMot.A.Ap <- resid(lmrob(AgencyAp ~ 1 + nwords, data = pact.dat, na.action=na.exclude, setting = "KS2014"))
pact.dat$IMot.A.Av <- resid(lmrob(AgencyAv ~ 1 + nwords, data = pact.dat, na.action=na.exclude, setting = "KS2014"))
pact.dat$IMot.A.Pow <- resid(lmrob(AgencyPow ~ 1 + nwords, data = pact.dat, na.action=na.exclude, setting = "KS2014"))
pact.dat$IMot.A.Ind <- resid(lmrob(AgencyInd ~ 1 + nwords, data = pact.dat, na.action=na.exclude, setting = "KS2014"))

# ---------------------------------------------------------------------
# add empty rows for couple where both partners wrote the same story 

pact.not.code_newd$IMot.C <- NA
pact.not.code_newd$IMot.C.Ap <- NA
pact.not.code_newd$IMot.C.Av <- NA
pact.not.code_newd$IMot.A <- NA
pact.not.code_newd$IMot.A.Ap <- NA
pact.not.code_newd$IMot.A.Av <- NA
pact.not.code_newd$IMot.A.Pow <- NA
pact.not.code_newd$IMot.A.Ind <- NA

pact.dat <- rbind(pact.dat, pact.not.code_newd)

# ---------------------------------------------------------------------
# z-standardize implicit motives

pact.dat <- pact.dat %>% mutate(
  Agency.z = (pact.dat$Agency - mean(pact.dat$Agency, na.rm = TRUE))/sd(pact.dat$Agency, na.rm = TRUE),
  AgencyAp.z = (pact.dat$AgencyAp - mean(pact.dat$AgencyAp, na.rm = TRUE))/sd(pact.dat$AgencyAp, na.rm = TRUE),
  AgencyAv.z = (pact.dat$AgencyAv - mean(pact.dat$AgencyAv, na.rm = TRUE))/sd(pact.dat$AgencyAv, na.rm = TRUE),
  AgencyPow.z = (pact.dat$AgencyPow - mean(pact.dat$AgencyPow, na.rm = TRUE))/sd(pact.dat$AgencyPow, na.rm = TRUE),
  AgencyInd.z = (pact.dat$AgencyInd - mean(pact.dat$AgencyInd, na.rm = TRUE))/sd(pact.dat$AgencyInd, na.rm = TRUE),
  Communion.z = (pact.dat$Communion - mean(pact.dat$Communion, na.rm = TRUE))/sd(pact.dat$Communion, na.rm = TRUE),
  CommunionAp.z = (pact.dat$CommunionAp - mean(pact.dat$CommunionAp, na.rm = TRUE))/sd(pact.dat$CommunionAp, na.rm = TRUE),
  CommunionAv.z = (pact.dat$CommunionAv - mean(pact.dat$CommunionAv, na.rm = TRUE))/sd(pact.dat$CommunionAv, na.rm = TRUE),
  IMot.C.z = (pact.dat$IMot.C - mean(pact.dat$IMot.C, na.rm = TRUE))/sd(pact.dat$IMot.C, na.rm = TRUE),
  IMot.C.Ap.z = (pact.dat$IMot.C.Ap - mean(pact.dat$IMot.C.Ap, na.rm = TRUE))/sd(pact.dat$IMot.C.Ap, na.rm = TRUE),
  IMot.C.Av.z = (pact.dat$IMot.C.Av - mean(pact.dat$IMot.C.Av, na.rm = TRUE))/sd(pact.dat$IMot.C.Av, na.rm = TRUE),
  IMot.A.z = (pact.dat$IMot.A - mean(pact.dat$IMot.A, na.rm = TRUE))/sd(pact.dat$IMot.A, na.rm = TRUE),
  IMot.A.Ap.z = (pact.dat$IMot.A.Ap - mean(pact.dat$IMot.A.Ap, na.rm = TRUE))/sd(pact.dat$IMot.A.Ap, na.rm = TRUE),
  IMot.A.Av.z = (pact.dat$IMot.A.Av - mean(pact.dat$IMot.A.Av, na.rm = TRUE))/sd(pact.dat$IMot.A.Av, na.rm = TRUE),
  IMot.A.Pow.z = (pact.dat$IMot.A.Pow - mean(pact.dat$IMot.A.Pow, na.rm = TRUE))/sd(pact.dat$IMot.A.Pow, na.rm = TRUE),
  IMot.A.Ind.z = (pact.dat$IMot.A.Ind - mean(pact.dat$IMot.A.Ind, na.rm = TRUE))/sd(pact.dat$IMot.A.Ind, na.rm = TRUE)
) 

# ---------------------------------------------------------------------
# remove columns which we normally don't need

pact.dat <- pact.dat %>% select(-c(1:504, 513:522))

## ======================================================================
## merge preQ and PACT data
## ======================================================================

# ---------------------------------------------------------------------
# sort by participant id

preQ.dat <- preQ.dat %>% arrange(code_new)
pact.dat <- pact.dat %>% arrange(code_new)

# ---------------------------------------------------------------------
# merge

preQ.pact.dat <- merge(preQ.dat, pact.dat, by = "code_new")

# ---------------------------------------------------------------------
# rename code_new to participant_id, and paarid to couple_id

preQ.pact.dat <- preQ.pact.dat %>% rename(participant_id = code_new, couple_id = paarid)

## ======================================================================
## Save data files for analyses
## ======================================================================

save(preQ.pact.dat, file="../processed_data/S1.preQ.pact.dat.RData")

## ======================================================================
## temp: compare with local version
## ======================================================================
# 
# load(file="../processed_data/preQ.sens.dat.RData")
# setwd("C:/Users/Caro/LRZ Sync+Share/DFG_Motive/Eigene Studien/Pilot 4 (Nov 2016)/R/cache")
# load(file="preq.dat.full.RData")
# 
# preq.dat.old <- preq.dat.full %>% select(-c(234:737, 746:753, 764:779, "time_h", "time_min", "time_sek", "time_ges_sek", "Jahre", "code", "jbez", "birth"))
# preq.dat.new <- merge(preQ.pact.dat, preQ.sens.dat, by = c("session", "code_new"))
# preq.dat.new <- preq.dat.new %>% select(-c(205:228, "age", "code_new", "jbez", "birth"))
#  
# library(dataCompareR)
# comp <- rCompare(preq.dat.old, preq.dat.new, keys = "session")
# summary(comp)
