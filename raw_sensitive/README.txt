Due to the dyadic nature of the data set, we cannot make the data fully openly available. The data and materials for Sample 1 (https://doi.org/10.5160/psychdata.zrce16dy99) and Sample 2 (https://doi.org/10.5160/psychdata.zrce18mo99) are published as scientific use files, which restricts access to scientific users.

For reproducing the results, place the following raw data files from the PsychData repositories into this folder (/raw_sensitive):

couple_matching_data.csv
zrce16dy99_fd1.csv
zrce16dy99_fd2.csv
zrce16dy99_fd3.csv
zrce18mo99_fd1.tsv
zrce18mo99_fd2.tsv
zrce18mo99_fd3.tsv

