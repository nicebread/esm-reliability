library(dplyr)
library(psych)

# load preprocessed data sets from both studies
# THESE DYADIC DATA SETS CANNOT BE PROVIDED OPENLY FOR PRIVACY REASONS
load(file="processed_data/S1.RData")
load(file="processed_data/S2.RData")

length(unique(S1$couple_uid))
length(unique(S1$person_uid))

length(unique(S2$couple_uid))
length(unique(S2$person_uid))

## ======================================================================
## sample descriptives 
## ======================================================================

# study 1
# raw data is published as a scientific use file:
# Zygar, C., Hagemeyer, B., Pusch, S., & Schönbrodt, F.D. (2018). From motive dispositions to states to outcomes: Research data of an intensive experience sampling study on communal motivational dynamics in couples [Translated Title] (Version 1) [Files on CD-ROM]. Trier: Center for Research Data in Psychology: PsychData of the Leibniz Institute for Psychology Information ZPID. https://doi.org/10.5160/psychdata.zrce16dy99
# see https://www.psychdata.de/index.php?main=search&sub=browse&id=zrce16dy99&lang=ger

# for this analysis, we need to combine two data sets (the public demographic data and the (internal) sensitive demographic data). Therefore, this code block is skipped for public use:

if (1==0) {

load("../../Database/2016_DFG-Motive-1/processed_data/preQ.sens.dat.RData")
load("../../Database/2016_DFG-Motive-1/processed_data/preQ.pact.dat.RData")
load("../../Database/2016_DFG-Motive-1/processed_data/dat.wide.RData")
S1.demo0 <- merge(preQ.pact.dat, preQ.sens.dat, by="participant_id")

# reduce to participants who actually participated in the ESM study
S1.demo <- S1.demo0 %>% filter(participant_id %in% unique(dat.wide$participant_id))

S1.sample <- S1.demo %>% select(participant_id, sex, stud, bezstatus, jbez_original, mbez, kinder) %>% mutate(rel.length = (jbez_original * 12 + mbez) / 12)

table(S1.sample$sex) # must be 130
prop.table(table(S1.sample$sex)) # (1 = male, 2 = female)
prop.table(table(S1.sample$stud))
describe(S1.sample$rel.length)

describe(S1.sample$rel.length)
S1.sample[, "bezstatus"] <-
  factor(
    S1.sample$bezstatus,
    levels = c(1:5),
    labels = c(
      "offene Beziehung",
      "feste Beziehung",
      "verlobt",
      "verheiratet / in eingetragener Lebenspartnerschaft",
      "andere"
    )
  )
prop.table(table(S1.sample$bezstatus))
1 - unname(prop.table(table(S1.sample$bezstatus))[4])
table(S1.sample$kinder)

# ---------------------------------------------------------------------
#  study 2
# raw data is published as a scientific use file:
# Zygar-Hoffmann, C., Hagemeyer, B., Pusch, S., & Schönbrodt, F.D. (2020). Eine große Längsschnittstudie zu Motivation, Verhalten und Zufriedenheit von Paaren: Forschungsdaten einer vierwöchigen Experience-Sampling-Studie mit einer Vor-, Nach- und einjährigen Follow-up-Befragung. [A large longitudinal study on motivation, behavior and satisfaction in couples: Research data from a four-week experience sampling study with a pre-, post-, and one-year follow-up assessment.] (Version 1.0.0) [Daten und Dokumentation]. Trier: Psychologisches Datenarchiv PsychData des Leibniz-Zentrums für Psychologische Information und Dokumentation ZPID. https://doi.org/10.5160/psychdata.zrce18mo99

# for this analysis, we need to combine two data sets (the public demographic data and the (internal) sensitive demographic data)

load("../../Database/2017-2019_DFG-Motive-2/processed_data/preQ.pact.dat.RData")
load("../../Database/2017-2019_DFG-Motive-2/processed_data/preQ.sens.dat.RData")
load("../../Database/2017-2019_DFG-Motive-2/processed_data/esm.dat.wide.RData")
S2.demo0 <- merge(preQ.pact.dat, preQ.sens.dat, by = c("participant_id"))

# reduce to participants who actually participated in the ESM study
S2.demo <- S2.demo0 %>% filter(participant_id %in% unique(esm.dat.wide$participant_id))

S2.sample <- S2.demo %>% select(participant_id, sex, age, stud, schule, schule_o, jbez, mbez, bezstatus, kinder) %>% mutate(rel.length = (jbez * 12 + mbez) / 12)

table(S2.sample$sex)
prop.table(table(S2.sample$sex)) # (1 = male, 2 = female)
prop.table(table(S2.sample$stud))
table(S2.sample$schule)
S2.sample$schule_o[S2.sample$schule_o != ""] # 5 additional "Abitur" (-> university degree)

describe(S2.sample$age)

describe(S2.sample$rel.length)
prop.table(table(S2.sample$bezstatus))
1 - unname(prop.table(table(S2.sample$bezstatus))[4])
prop.table(table(S2.sample$kinder))

} # of if (1==0)

## ======================================================================
## scheduled vs. answered pings, compliance rate
## ======================================================================

load(file="processed_data/S1.long.RData")
load(file="processed_data/S2.long.RData")

# scheduled pings: 

# S1:
130*14*5

# S2:
508*28*4 #(S2: only 4 pings for motivation)
508*28*5 #(S2: 5 pings for relsat)

# moments in data set:
length(unique(S1.C4.long$person_moment_uid))  # 100%. This is not 9100 (the full number of scheduled pings, because we had some a priori exclusions due to Christmas break, and one person starting later than expected)
length(unique(S2.C4.long$person_moment_uid))  # 100%

# non-NA moments in data set. We need these numbers for the Descriptives table
S1.C4.pings <- S1.C4.long[!is.na(S1.C4.long$value), "person_moment_uid"] %>% unique %>% nrow
S1.Ind.pings <- S1.Ind.long[!is.na(S1.Ind.long$value), "person_moment_uid"] %>% unique %>% nrow
S1.Pow.pings <- S1.Pow.long[!is.na(S1.Pow.long$value), "person_moment_uid"] %>% unique %>% nrow
S1.A.pings <- S1.A.long[!is.na(S1.A.long$value), "person_moment_uid"] %>% unique %>% nrow
S1.RS.pings <- S1.RS2.long[!is.na(S1.RS2.long$value), "person_moment_uid"] %>% unique %>% nrow

S2.C4.pings <- S2.C4.long[!is.na(S2.C4.long$value), "person_moment_uid"] %>% unique %>% nrow
S2.Ind.pings <- S2.Ind.long[!is.na(S2.Ind.long$value), "person_moment_uid"] %>% unique %>% nrow
S2.Pow.pings <- S2.Pow.long[!is.na(S2.Pow.long$value), "person_moment_uid"] %>% unique %>% nrow
S2.A.pings <- S2.A.long[!is.na(S2.A.long$value), "person_moment_uid"] %>% unique %>% nrow
S2.RS2.pings <- S2.RS2.long[!is.na(S2.RS2.long$value), "person_moment_uid"] %>% unique %>% nrow
S2.RS3.pings <- S2.RS3.long[!is.na(S2.RS3.long$value), "person_moment_uid"] %>% unique %>% nrow

# What is the minimum number of actually answered pings in the analyses? (Depends on missing values in the surveys)
min(S1.C4.pings, S1.Ind.pings, S1.Pow.pings, S1.A.pings, S1.RS.pings)
min(S2.C4.pings, S2.Ind.pings, S2.Pow.pings, S2.A.pings, S2.RS2.pings, S2.RS3.pings)

# Generell würde mir irgendwo (z.B. in den R-Skripten) ein Memo gut tun, warum die Zahlen hier anders sind als in den anderen Papern:
# Bei Studie 1 ist der Unterschied zwischen 7508 (hier) und 7573 (Retro) auf begonnene aber nicht die Motivation-Items enthaltene Surveys zurückzuführen.
# Bei Studie 2 ist der Unterschied zwischen 47764 (hier) und 60942 (Retro) zusätzlich darauf zurückzuführen, dass die abendbefragungen für die motivationsfragen komplett rausfallen (-20%), sowie das eine extra ausgeschlossene Paar (couple_id = 156)

save(S2.C4.pings, S2.Ind.pings, S2.Pow.pings, S2.A.pings, S2.RS2.pings, S2.RS3.pings, S1.C4.pings, S1.Ind.pings, S1.Pow.pings, S1.A.pings, S1.RS.pings, file="processed_data/n_survey.RData")


## ======================================================================
## Sanity check: Why do not all scales have the exact same count of answered surveys?
## Answer: Incomplete/interrupted surveys where participants submitted only partly complete data.
## ======================================================================

inc <- which(is.na(dat.wide$Mot.A3) & !is.na(dat.wide$Mot.A1))

dat.wide[inc, ] %>% select(Mot.A1, Mot.A3, EventStartedTime, EventCompletedTime)


## ======================================================================
## Sanity check: compare descriptives with MotBeh paper
## ======================================================================

# scale of 2 items
S2 %>% mutate(
	C2.raw = (C_1 + C_2)/2
) %>% 
group_by(person_uid) %>% summarize(
	C2.M = mean(C2.raw, na.rm=TRUE),
	C_3.M = mean(C_3, na.rm=TRUE),
	C_4.M = mean(C_4, na.rm=TRUE)
) %>% ungroup() %>% summarize(
	C2.MM = mean(C2.M),
	C_3.MM = mean(C_3.M),
	C_4.MM = mean(C_4.M)
)
