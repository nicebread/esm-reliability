# Tutorial for nondyadic reliability analysis

*Felix Schönbrodt, 2022-01-28*

Do you want to run a reliability estimation on non-dyadic data, where
your data has moments within days within persons? As described in the
paper, the formulas can be easily adapted to this situation. For
measurement designs without a dyadic structure on the highest level,
reliability formulas R<sub>BP</sub>, R<sub>WPD</sub>, and
R<sub>WPM</sub> are identical. In this case, the variance decomposition
in Eq. (1) simply omits all terms including the factor C.

## Creating a non-dyadic data set

For this demonstration, we take the data of the male participants from
study 1 (relationship satisfaction measured with 2 items) to create a
non-dyadic data set. `varDecomp` is a helper function that extracts the
variance components from the `lmer` object.

``` r
load("processed_data/S1.long.RData")
load("processed_data/S1.RData")

male_person_uids <- unique(S1$person_uid[S1$sex_m == 1])
males <- S1.RS2.long[S1.RS2.long$person_uid %in% male_person_uids, ] %>% 
    select(-couple_uid, -couple_moment_uid, -couple_studyday_uid, -weekend)

# helper function (simplified version from /helpers/Variance decomposition.R)
varDecomp <- function(model) {
    VAR <- as.data.frame(lme4::VarCorr(model))
    res_var <- VAR$vcov[VAR$grp=="Residual"]    # residual variance on L1
    variances <- VAR[VAR$grp!="Residual", "vcov"]
    names(variances) <- VAR[VAR$grp!="Residual", "grp"]
    stable_variance <- sum(variances)
    full_variance <- stable_variance+res_var

    res <- c(variances, Error=res_var)
      
    # Danger zone!! lmer changes the order of coefficients, even if the model syntax is the same. 
    # Bring into alphabetical order, except "error" which is last.
    res0 <- res[order(names(res))]
    res <- c(res0[names(res0) != "Error"], res["Error"])
    
    return(res)
}    
```

## Syntax for non-dyadic variance decomposition

``` r
#------------------------------------------------------------------------------
#  Full factorial model for nondyadic data (but still moments in days in persons)
#------------------------------------------------------------------------------
# We print the full model with all interactions, and comment out all terms
# that make no sense / cannot be estimated.
#
## Coding of indicator variables
# Indexes for day are repeated within each person, and indexes for moment are repeated within each day of each person 
# (as they are crossed). Indexes for couple and person, in contrast, are unique for each couple and each person in the 
# sample (\emph{uid} = unique id), as persons are nested in couples.
# For example, studyday_id runs from 1 to 14 in a two-week study, and these numbers are repeated within each person.
## ======================================================================


VDC.nondyadic.syntax <- formula(
  
    value ~ 1 +
  
  # main effects
    (1 | person_uid) +
    (1 | studyday_id) +
    (1 | moment_id) +
    (1 | item) +

   # two-way IA
    (1 | person_uid:studyday_id) +
    (1 | person_uid:moment_id) +
    (1 | person_uid:item) +

    (1 | studyday_id:moment_id) +
    (1 | studyday_id:item) +

    (1 | moment_id:item) +

    # three-way IA  
    (1 | person_uid:studyday_id:moment_id) +
    (1 | person_uid:studyday_id:item) +
    (1 | person_uid:moment_id:item) +
    (1 | studyday_id:moment_id:item)

    # four-way IA   
    # (1|person_uid:studyday_id:moment_id:item)         # not defined (only one data point in this factor)
)
```

``` r
library(lmerTest)
VDC_nondyadic <- lmer(formula=VDC.nondyadic.syntax, data=males, REML=FALSE)
summary(VDC_nondyadic)
```

    ## Linear mixed model fit by maximum likelihood . t-tests use Satterthwaite's
    ##   method [lmerModLmerTest]
    ## Formula: VDC.nondyadic.syntax
    ##    Data: males
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##  16395.2  16504.9  -8181.6  16363.2     7015 
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -6.1730 -0.3041  0.0681  0.4188  2.9180 
    ## 
    ## Random effects:
    ##  Groups                           Name        Variance  Std.Dev. 
    ##  person_uid:studyday_id:moment_id (Intercept) 1.994e-01 4.465e-01
    ##  person_uid:studyday_id:item      (Intercept) 6.468e-02 2.543e-01
    ##  person_uid:studyday_id           (Intercept) 9.204e-02 3.034e-01
    ##  person_uid:moment_id:item        (Intercept) 1.708e-10 1.307e-05
    ##  person_uid:moment_id             (Intercept) 8.051e-11 8.973e-06
    ##  studyday_id:moment_id:item       (Intercept) 1.165e-03 3.414e-02
    ##  person_uid:item                  (Intercept) 1.665e-01 4.081e-01
    ##  studyday_id:moment_id            (Intercept) 3.795e-11 6.160e-06
    ##  person_uid                       (Intercept) 1.421e-01 3.770e-01
    ##  studyday_id:item                 (Intercept) 4.864e-04 2.205e-02
    ##  studyday_id                      (Intercept) 1.751e-03 4.185e-02
    ##  moment_id:item                   (Intercept) 1.274e-03 3.569e-02
    ##  moment_id                        (Intercept) 0.000e+00 0.000e+00
    ##  item                             (Intercept) 0.000e+00 0.000e+00
    ##  Residual                                     3.151e-01 5.613e-01
    ## Number of obs: 7031, groups:  
    ## person_uid:studyday_id:moment_id, 3543; person_uid:studyday_id:item, 1733; person_uid:studyday_id, 868; person_uid:moment_id:item, 628; person_uid:moment_id, 314; studyday_id:moment_id:item, 140; person_uid:item, 126; studyday_id:moment_id, 70; person_uid, 63; studyday_id:item, 28; studyday_id, 14; moment_id:item, 10; moment_id, 5; item, 2
    ## 
    ## Fixed effects:
    ##             Estimate Std. Error       df t value Pr(>|t|)
    ## (Intercept) -0.05596    0.06411 67.55367  -0.873    0.386
    ## optimizer (nloptwrap) convergence code: 0 (OK)
    ## boundary (singular) fit: see ?isSingular

``` r
# extract the variance components
VAR <- varDecomp(VDC_nondyadic)
round(VAR, 3)
```

    ##                             item                        moment_id 
    ##                            0.000                            0.000 
    ##                   moment_id:item                       person_uid 
    ##                            0.001                            0.142 
    ##                  person_uid:item             person_uid:moment_id 
    ##                            0.167                            0.000 
    ##        person_uid:moment_id:item           person_uid:studyday_id 
    ##                            0.000                            0.092 
    ##      person_uid:studyday_id:item person_uid:studyday_id:moment_id 
    ##                            0.065                            0.199 
    ##                      studyday_id                 studyday_id:item 
    ##                            0.002                            0.000 
    ##            studyday_id:moment_id       studyday_id:moment_id:item 
    ##                            0.000                            0.001 
    ##                            Error 
    ##                            0.315

If you compare these numbers to the numbers from the dyadic analyses
(which include the associated women in each couple; see Table 5 in the
[publication](https://psyarxiv.com/6mq7t)), you see that they generally
are very close. Some variances are shifted from the couple level to the
respective person level, which is exactly what we would expect when the
couple level is omitted from the analysis.

For example, the combined *couple:day* and *person:day* variances from
the dyadic analysis (.09 and .01) are close to the *person:day* variance
in the individual analysis (.09); another example is *couple:item* (.04)
and *person:item* (.12), which is close to the *person:item* variance of
the individual analysis (.17). (Remember that we only analyzed the male
half of the data set, so we would not expect that the variances
perfectly sum up here).

## Reliability estimates

``` r
  # assign easier names to variances
  s_P <- VAR["person_uid"]
  s_PI <- VAR["person_uid:item"]
  s_D <- VAR["studyday_id"]
  s_DI <- VAR["studyday_id:item"]
  s_PD <- VAR["person_uid:studyday_id"]
  s_PM <- VAR["person_uid:moment_id"]
  s_PMI <- VAR["person_uid:moment_id:item"]
  s_DMI <- VAR["studyday_id:moment_id:item"]
  s_DM <- VAR["studyday_id:moment_id"]
  s_PDI <- VAR["person_uid:studyday_id:item"]
  s_PDM <- VAR["person_uid:studyday_id:moment_id"]
  s_e <- VAR["Error"]

  # define the number of replications (for simplicity, we take here the number of scheduled items etc., not the number 
  # of actually answered items)
  ## notation: 
  # n_i = number of items (in the manuscript, this is j)
  # n_d = number of days within person (in the manuscript, this is k)
  # n_m = number of moments within day (in the manuscript, this is l)

  n_i = 2
  n_m = 5
  n_d = 14

  # Between person:
  # This is formula (8) R_KR (p. 311) from Lane & Shrout (2012, with adding the moment level)
  R_BP <- (s_P + s_PI / n_i + s_PM / n_m + s_PMI / (n_m * n_i)) /
          (s_P + s_PI / n_i + s_PM / n_m + s_PMI / (n_m * n_i) +
          s_D / n_d + s_PD / n_d + s_DM / (n_d * n_m) + s_DI / (n_d * n_i) +
          s_PDM / (n_d * n_m) + s_PDI / (n_d * n_i) + s_DMI / (n_d * n_m * n_i) + s_e / (n_i * n_m * n_d))


  # Between days: focal term = PD (i.e., one day)
  R_WPD <- (s_PD + s_PDI / n_i + s_PDM / n_m) /
           (s_PD + s_PDI / n_i + s_PDM / n_m +
            s_D + s_DM / n_m + s_DI / n_i + s_DMI / (n_m * n_i) + s_e / (n_i * n_m))

  # Between moments
  R_WPM <- (s_PDM) / (s_PDM + s_e / n_i)
```

|        | dyadic\_reliabilities | individual\_reliabilities |
|:-------|----------------------:|--------------------------:|
| R\_BP  |                  0.96 |                      0.94 |
| R\_WPD |                  0.64 |                      0.83 |
| R\_WPM |                  0.36 |                      0.56 |

The table compares the original dyadic reliabilities (when women and men
are analyzed together) with the newly computated reliabilities. (Note:
For simplicity, we took the number of scheduled items etc., not the
number of actually answered items. Therefore the numbers of the dyadic
reliabilities do not exactly match the numbers reported in Table 7 the
publication; see also Footnote 4 therein).

Compared to the dyadic reliability analyses, these estimates show
partially higher reliabilities. This can be attributed to the fact that
variance on the couple level is relocated to the person level, which
increases the relative stable variances. (Both the couple main effect,
and all interactions of couple with other factors).

Hence, the dyadic analysis is more conservative, as it computes a
between-person reliability that is also able to differentiate persons
within a couple, on all temporal levels. As persons within a couple tend
to be more similar (at least for the constructs we looked at in the
paper), it is harder to differentiate them. If we only look at the male
partners, between-couple differences *are* between-person differences!
Hence, it is plausible and expectable that the reliability estimates are
higher.
