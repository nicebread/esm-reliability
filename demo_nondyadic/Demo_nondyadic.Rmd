---
title: 'Demo of nondyadic analysis'
author: 'Felix Schönbrodt'
date: '`r format(Sys.Date())`'
output:
  md_document:
    variant: gfm
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, cache=TRUE)
knitr::opts_knit$set(root.dir = "..")
library(dplyr)
```

# Tutorial for nondyadic reliability analysis
*Felix Schönbrodt, `r format(Sys.Date())`*

Do you want to run a reliability estimation on non-dyadic data, where your data has moments within days within persons? As described in the paper, the formulas can be easily adapted to this situation. For measurement designs without a dyadic structure on the highest level, reliability formulas R~BP~, R~WPD~, and R~WPM~ are identical. In this case, the variance decomposition in Eq. (1) simply omits all terms including the factor C.

## Creating a non-dyadic data set

For this demonstration, we take the data of the male participants from study 1 (relationship satisfaction measured with 2 items) to create a non-dyadic data set.
`varDecomp` is a helper function that extracts the variance components from the `lmer` object.

```{r create_data_set, echo=TRUE}
load("processed_data/S1.long.RData")
load("processed_data/S1.RData")

male_person_uids <- unique(S1$person_uid[S1$sex_m == 1])
males <- S1.RS2.long[S1.RS2.long$person_uid %in% male_person_uids, ] %>% 
    select(-couple_uid, -couple_moment_uid, -couple_studyday_uid, -weekend)

# helper function (simplified version from /helpers/Variance decomposition.R)
varDecomp <- function(model) {
	VAR <- as.data.frame(lme4::VarCorr(model))
	res_var <- VAR$vcov[VAR$grp=="Residual"]	# residual variance on L1
	variances <- VAR[VAR$grp!="Residual", "vcov"]
	names(variances) <- VAR[VAR$grp!="Residual", "grp"]
	stable_variance <- sum(variances)
	full_variance <- stable_variance+res_var

    res <- c(variances, Error=res_var)
      
    # Danger zone!! lmer changes the order of coefficients, even if the model syntax is the same. 
    # Bring into alphabetical order, except "error" which is last.
    res0 <- res[order(names(res))]
    res <- c(res0[names(res0) != "Error"], res["Error"])
	
	return(res)
}    
```



## Syntax for non-dyadic variance decomposition

```{r syntax, echo=TRUE}

#------------------------------------------------------------------------------
#  Full factorial model for nondyadic data (but still moments in days in persons)
#------------------------------------------------------------------------------
# We print the full model with all interactions, and comment out all terms
# that make no sense / cannot be estimated.
#
## Coding of indicator variables
# Indexes for day are repeated within each person, and indexes for moment are repeated within each day of each person 
# (as they are crossed). Indexes for couple and person, in contrast, are unique for each couple and each person in the 
# sample (\emph{uid} = unique id), as persons are nested in couples.
# For example, studyday_id runs from 1 to 14 in a two-week study, and these numbers are repeated within each person.
## ======================================================================


VDC.nondyadic.syntax <- formula(
  
    value ~ 1 +
  
  # main effects
    (1 | person_uid) +
    (1 | studyday_id) +
    (1 | moment_id) +
    (1 | item) +

   # two-way IA
    (1 | person_uid:studyday_id) +
    (1 | person_uid:moment_id) +
    (1 | person_uid:item) +

    (1 | studyday_id:moment_id) +
    (1 | studyday_id:item) +

    (1 | moment_id:item) +

    # three-way IA	
    (1 | person_uid:studyday_id:moment_id) +
    (1 | person_uid:studyday_id:item) +
    (1 | person_uid:moment_id:item) +
    (1 | studyday_id:moment_id:item)

    # four-way IA	
    # (1|person_uid:studyday_id:moment_id:item)			# not defined (only one data point in this factor)
)
```


```{r, echo=TRUE, message=FALSE}
library(lmerTest)
VDC_nondyadic <- lmer(formula=VDC.nondyadic.syntax, data=males, REML=FALSE)
summary(VDC_nondyadic)

# extract the variance components
VAR <- varDecomp(VDC_nondyadic)
round(VAR, 3)
```

If you compare these numbers to the numbers from the dyadic analyses (which include the associated women in each couple; see Table 5 in the [publication](https://psyarxiv.com/6mq7t)), you see that they generally are very close. Some variances are shifted from the couple level to the respective person level, which is exactly what we would expect when the couple level is omitted from the analysis.

For example, the combined *couple:day* and *person:day* variances from the dyadic analysis (.09 and .01) are close to the *person:day* variance in the individual analysis (.09); another example is *couple:item* (.04) and *person:item* (.12), which is close to the *person:item* variance of the individual analysis (.17).
(Remember that we only analyzed the male half of the data set, so we would not expect that the variances perfectly sum up here).

## Reliability estimates

```{r, echo=TRUE}
  # assign easier names to variances
  s_P <- VAR["person_uid"]
  s_PI <- VAR["person_uid:item"]
  s_D <- VAR["studyday_id"]
  s_DI <- VAR["studyday_id:item"]
  s_PD <- VAR["person_uid:studyday_id"]
  s_PM <- VAR["person_uid:moment_id"]
  s_PMI <- VAR["person_uid:moment_id:item"]
  s_DMI <- VAR["studyday_id:moment_id:item"]
  s_DM <- VAR["studyday_id:moment_id"]
  s_PDI <- VAR["person_uid:studyday_id:item"]
  s_PDM <- VAR["person_uid:studyday_id:moment_id"]
  s_e <- VAR["Error"]

  # define the number of replications (for simplicity, we take here the number of scheduled items etc., not the number 
  # of actually answered items)
  ## notation: 
  # n_i = number of items (in the manuscript, this is j)
  # n_d = number of days within person (in the manuscript, this is k)
  # n_m = number of moments within day (in the manuscript, this is l)

  n_i = 2
  n_m = 5
  n_d = 14

  # Between person:
  # This is formula (8) R_KR (p. 311) from Lane & Shrout (2012, with adding the moment level)
  R_BP <- (s_P + s_PI / n_i + s_PM / n_m + s_PMI / (n_m * n_i)) /
          (s_P + s_PI / n_i + s_PM / n_m + s_PMI / (n_m * n_i) +
          s_D / n_d + s_PD / n_d + s_DM / (n_d * n_m) + s_DI / (n_d * n_i) +
          s_PDM / (n_d * n_m) + s_PDI / (n_d * n_i) + s_DMI / (n_d * n_m * n_i) + s_e / (n_i * n_m * n_d))


  # Between days: focal term = PD (i.e., one day)
  R_WPD <- (s_PD + s_PDI / n_i + s_PDM / n_m) /
           (s_PD + s_PDI / n_i + s_PDM / n_m +
            s_D + s_DM / n_m + s_DI / n_i + s_DMI / (n_m * n_i) + s_e / (n_i * n_m))

  # Between moments
  R_WPM <- (s_PDM) / (s_PDM + s_e / n_i)
```

```{r, echo=FALSE}
dyadic_reliabilities <- c(.96, .64, .36)
individual_reliabilities <- round(c(R_BP, R_WPD, R_WPM), 2)
rel_table <- cbind(dyadic_reliabilities, individual_reliabilities)
rownames(rel_table) <- c("R_BP", "R_WPD", "R_WPM")
knitr::kable(rel_table)
```

The table compares the original dyadic reliabilities (when women and men are analyzed together) with the newly computated reliabilities. (Note: For simplicity,  we took the number of scheduled items etc., not the number of actually answered items. Therefore the numbers of the dyadic reliabilities do not exactly match the numbers reported in Table 7 the publication; see also Footnote 4 therein).

Compared to the dyadic reliability analyses, these estimates show partially higher reliabilities. This can be attributed to the fact that variance on the couple level is relocated to the person level, which increases the relative stable variances. (Both the couple main effect, and all interactions of couple with other factors).

Hence, the dyadic analysis is more conservative, as it computes a between-person reliability that is also able to differentiate persons within a couple, on all temporal levels. As persons within a couple tend to be more similar (at least for the constructs we looked at in the paper), it is harder to differentiate them. If we only look at the male partners, between-couple differences *are* between-person differences! Hence, it is plausible and expectable that the reliability estimates are higher.