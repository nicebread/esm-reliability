#------------------------------------------------------------------------------
#  Full factorial model for nondyadic data (but still moments in days in persons)
#------------------------------------------------------------------------------
# We print the full model with all interactions, and comment out all terms
# that make no sense / cannot be estimated.
#
## Coding of indicator variables
# Indexes for day are repeated within each person, and indexes for moment are repeated within each day of each person (as they are crossed). Indexes for couple and person, in contrast, are unique for each couple and each person in the sample (\emph{uid} = unique id), as persons are nested in couples.
# For example, studyday_id runs from 1 to 14 in a two-week study, and these numbers are repeated within each person.
## ======================================================================


VDC.nondyadic.syntax <- formula(
  
    value ~ 1 +
  
  # main effects
    (1 | person_uid) +
    (1 | studyday_id) +
    (1 | moment_id) +
    (1 | item) +

   # two-way IA
    (1 | person_uid:studyday_id) +
    (1 | person_uid:moment_id) +
    (1 | person_uid:item) +

    (1 | studyday_id:moment_id) +
    (1 | studyday_id:item) +

    (1 | moment_id:item) +

    # three-way IA	
    (1 | person_uid:studyday_id:moment_id) +
    (1 | person_uid:studyday_id:item) +
    (1 | person_uid:moment_id:item) +
    (1 | studyday_id:moment_id:item)

    # four-way IA	
    # (1|person_uid:studyday_id:moment_id:item)			# not defined (only one data point in this factor)
)